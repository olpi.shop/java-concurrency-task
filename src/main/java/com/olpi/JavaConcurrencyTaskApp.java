package com.olpi;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.olpi.domain.Matrix;
import com.olpi.threads.MatrixThread;

public class JavaConcurrencyTaskApp {

    private static final Logger logger = LogManager
            .getLogger(JavaConcurrencyTaskApp.class);

    public static void main(String[] args) {

        logger.debug("Start Java Concurrency App!");

        Random random = new Random();

        File fileForMatrix = new File(
                "./src/main/resources/initFiles/NumbersForMatrix.txt");
        File fileForThreads = new File(
                "./src/main/resources/initFiles/UniqueNumbersForThreads.txt");

        int minSizeOfMatrix = 8;
        int maxSizeOfMatrix = 12;
        int sizeOfMatrix = random.nextInt(maxSizeOfMatrix - minSizeOfMatrix + 1)
                + minSizeOfMatrix;

        int minAmountOfThreads = 4;
        int maxAmountOfThreads = 6;
        int amountOfThreads = random
                .nextInt(maxAmountOfThreads - minAmountOfThreads + 1)
                + minAmountOfThreads;

        try (Scanner scan = new Scanner(fileForThreads)) {

            Matrix matrix = new Matrix(sizeOfMatrix);
            logger.info("Matrix has created");

            matrix.initializeMatrixExceptMainDiagonal(fileForMatrix);

            for (int i = 0; i < amountOfThreads; i++) {
                int numberForThread = scan.nextInt();
                logger.debug("Create new thread with the number: {}",
                        numberForThread);
                new MatrixThread(matrix, numberForThread).start();
            }

            TimeUnit.SECONDS.sleep(2);
            logger.info("Matrix has initialized");

            System.out.println("\nResult:\n" + matrix.toString());

            logger.debug("Application has completed!");

        } catch (FileNotFoundException ex) {
            logger.log(Level.ERROR, " File not found ", ex);
        } catch (InterruptedException ex) {
            logger.log(Level.WARN, "Interrupted!", ex);
            Thread.currentThread().interrupt();
        }
    }
}
