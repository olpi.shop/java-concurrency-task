package com.olpi.threads;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.olpi.domain.Matrix;

public class MatrixThread extends Thread {

    private static final Logger logger = LogManager
            .getLogger(MatrixThread.class);

    private final int number;
    private final Matrix matrix;

    public MatrixThread(Matrix matrix, int number) {
        this.matrix = matrix;
        this.number = number;
    }

    @Override
    public void run() {
        try {
            TimeUnit.MILLISECONDS.sleep(10);
        } catch (InterruptedException ex) {
            logger.log(Level.WARN, "Interrupted!", ex);
            Thread.currentThread().interrupt();
        }
        matrix.initializeMainDiagonal(number);
    }

}
