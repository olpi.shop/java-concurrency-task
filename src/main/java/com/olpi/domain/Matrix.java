package com.olpi.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Matrix {

    private static final Logger logger = LogManager.getLogger(Matrix.class);

    private final int sizeOfMatrix;
    private final int arrayOfMatrix[][];

    private final Lock lock = new ReentrantLock(true);
    private final Condition notMultithreaded = lock.newCondition();

    public Matrix(int size) {
        this.sizeOfMatrix = size;
        arrayOfMatrix = new int[size][size];
    }

    public void initializeMatrixExceptMainDiagonal(File initializationFile)
            throws FileNotFoundException {
        logger.debug("Initialize matrix from file {}",
                initializationFile.getName());

        Scanner scan = new Scanner(initializationFile);

        for (int i = 0; i < sizeOfMatrix; i++) {
            for (int j = 0; j < sizeOfMatrix; j++) {

                if (i != j) {
                    arrayOfMatrix[i][j] = scan.nextInt();
                    logger.debug(
                            "The element of matrix on [{}][{}] position = {}",
                            i, j, arrayOfMatrix[i][j]);
                }
                if (!scan.hasNext()) {
                    logger.debug("The file is over, scanning again");
                    scan.close();
                    scan = new Scanner(initializationFile);
                }
            }
        }
        scan.close();
    }

    public void initializeMainDiagonal(int newNumber) {
        logger.debug("Initialize main diagonal with number = {}", newNumber);

        try {
            lock.lock();
            logger.debug("Matrix is locked");

            for (int i = 0; i < sizeOfMatrix; i++) {
                notMultithreaded.signal();

                if (arrayOfMatrix[i][i] == 0) {
                    arrayOfMatrix[i][i] = newNumber;
                    logger.debug(
                            "The element of diagonal on [{}][{}] position = {}",
                            i, i, arrayOfMatrix[i][i]);

                    if (i < sizeOfMatrix - 1) {
                        while (arrayOfMatrix[i + 1][i + 1] == 0) {
                            logger.debug("Current thread is waiting");
                            notMultithreaded.await();
                        }
                    }
                }
            }
        } catch (InterruptedException ex) {
            logger.log(Level.WARN, "Interrupted!", ex);
            Thread.currentThread().interrupt();
        } finally {
            logger.debug("Matrix unlocked");
            lock.unlock();
        }
    }

    public String toString() {

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < sizeOfMatrix; i++) {
            for (int j = 0; j < sizeOfMatrix; j++) {
                builder.append(String.format(" %-3d ", arrayOfMatrix[i][j]));
            }
            builder.append("\n");
        }
        return builder.toString();
    }

}
